#!/usr/bin/env ruby 
 
replace="
            </intent-filter>
            <intent-filter>
                <action android:name='android.intent.action.SEND' />
                <action android:name='android.intent.action.SEND_MULTIPLE'/>
                <category android:name='android.intent.category.DEFAULT' />
                <data android:mimeType='*/*' />
    "
 
filename = "platforms/android/AndroidManifest.xml"
outdata = File.read(filename).gsub(/<category android\:name=\"android\.intent\.category\.LAUNCHER\" \/>([^<]*)<\/intent-filter>([^<]*)<\/activity>/, "<category android\:name=\"android\.intent\.category\.LAUNCHER\" \/>#{replace}<\/intent-filter><\/activity>")
 
File.open(filename, 'w') do |out|
    out << outdata
end
