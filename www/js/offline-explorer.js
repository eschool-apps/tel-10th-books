var offlineExplorer = {
  markFileforDownloading : function(uuid, category_path) {
    console.log("markFileforDownloading category_path "+category_path);
    console.log("markFileforDownloading uuid "+ uuid);
    /*
     * Check if DownloadDestination is set in settings
     */
    if (!sdcardLoc) {
      navigator.notification.alert("Set the Download destination path in Settings");
      return false;
    }
    /*
     * TODO: If the uuid already exists in download table, notify the user, reset download_attempts column if necessary.
     * mark downloaded column as 0 - pending
     * mark error column as 0, If error > 3 stop trying to download
     * get data from API for the specified uuid
     * id, uuid, unified_path, doc.type, download_url, doc.offline_file, thumbnail_url, offline_thumbnail, doc.title, description, downloaded(0/1)
     */
    databaseApp.db
    .transaction(function(transaction) {
      transaction
          .executeSql(
              "SELECT id FROM cache_yaml_files WHERE uuid = '"+uuid+"'",
              [],
              function(tx, results) {
                console.log("checking If the uuid already exists in download table "+ results.rows.length);
                if(results.rows.length > 0){
                  databaseApp.updateYAMLresetDownloadAttemptsRecord(uuid);
                  navigator.notification.alert("Already in download queue.");
                }else{
                  // New item to download
                  var api_url = API_URL + category_path;
                  console.log(api_url);
                  $.ajax({
                    url : encodeURI(api_url),
                    dataType : 'JSONP',
                    jsonpCallback : 'callback',
                    type : 'GET',
                    success : function(pagesObj) {
                      //console.log(pagesObj);
                      $.each(pagesObj, function(i, el) {
                        if (el.uuid == uuid) {
                          console.log(JSON.stringify(el));
                          console.log(el.download_url);
                          var first_thumbnail_url = '';
                          if(el.thumbnails && el.thumbnails[0]){
                            console.log(el.thumbnails[0]);
                            first_thumbnail_url = el.thumbnails[0];
                          }
                          // Get extension from download_url
                          var file_name = el.download_url;
                          var extension = file_name.substr(file_name.lastIndexOf('.') + 1);
                          console.log("bbb extension "+extension);
                          // function(uuid, path, extension, type, offline_file, display_name, description, download_url, thumbnail_url, offline_thumbnail, downloaded, download_attempts)
                          databaseApp.insertYAMLrecord(el.uuid, el.category_path, extension,
                              el.type, '', el.title, el.content, el.download_url, first_thumbnail_url, '', 0, 0);
                          /*
                           * alert the user that download request has been added to queue and the download will begin as soon as possible.
                           */
                          navigator.notification.alert("Added to download queue. Download will begin as soon as possible.");
                          //return false;
                        }
                      });
                    }
                  });
                }
              }, databaseApp.onError );
      }, databaseApp.onError);
  }
};