var apiServicesApp = {
  createCategory : function(parentPath, categoryName) {
    var postData = {};
    postData["client"] = {
      "id" : "dummy",
      "project uuid" : PROJECT_UUID
    };
    postData["category"] = {
        "parent path" : parentPath,
        "new category" : categoryName
      };
    console.log("adarsh: postData: "
        + JSON.stringify(postData));
    $.ajax({
      url : "http://eschool2go.org/api/v2/category",
      type : "POST",
      dataType: 'json',
      data : JSON.stringify(postData),
      contentType : "application/json; charset=utf-8",
      beforeSend: function (xhr) {
        // https://github.com/bettiolo/oauth-signature-js#example
        var oauth_auth_header = apiServicesApp.make_oauth_auth_header("http://eschool2go.org/api/v2/category", 'POST');
        console.log("adarsh oauth_auth_header "+ oauth_auth_header);
        xhr.setRequestHeader('Authorization', oauth_auth_header);
      },
      success : function(data) {
        console.log("adarsh: uploaadAnalyticsEvent sent. Data Loaded: "
            + JSON.stringify(data));
      },
      error : function(XMLHttpRequest, textStatus, errorThrown) {
        console.log("adarsh: uploaadAnalyticsEvent post error:textStatus " + textStatus + errorThrown);
        //console.log("adarsh: uploaadAnalyticsEvent post error: textStatus " + textStatus);
      }
    });
  },
  
  createArticle : function(link, name, path) {
    var postData = {};
    postData["client"] = {
      "id" : "dummy",
      "project uuid" : PROJECT_UUID
    };
    postData["article"] = {
        "link" : link,
        "path" : path,
        "name" : name
      };
    console.log("adarsh: postData: "
        + JSON.stringify(postData));
    $.ajax({
      url : "http://eschool2go.org/api/v2/article",
      type : "POST",
      dataType: 'json',
      data : JSON.stringify(postData),
      contentType : "application/json; charset=utf-8",
      beforeSend: function (xhr) {
        // https://github.com/bettiolo/oauth-signature-js#example
        var oauth_auth_header = apiServicesApp.make_oauth_auth_header("http://eschool2go.org/api/v2/article", 'POST');
        console.log("adarsh oauth_auth_header "+ oauth_auth_header);
        xhr.setRequestHeader('Authorization', oauth_auth_header);
      },
      success : function(data) {
        console.log("adarsh: uploaadAnalyticsEvent sent. Data Loaded: "
            + JSON.stringify(data));
      },
      error : function(XMLHttpRequest, textStatus, errorThrown) {
        console.log("adarsh: uploaadAnalyticsEvent post error:textStatus " + textStatus + errorThrown);
        //console.log("adarsh: uploaadAnalyticsEvent post error: textStatus " + textStatus);
      }
    });
  },
  
  make_oauth_auth_header : function(realm, httpMethod) {
    if(isAuthenticatedUser){
      var nonce = Math.random();
      var timestamp = Math.round(new Date().getTime()/1000);
      var url = realm,
        parameters = {
          oauth_consumer_key : userConsumerKey,
          oauth_nonce : nonce,
          oauth_timestamp : timestamp,
          oauth_signature_method : 'HMAC-SHA1',
          oauth_version : '1.0',
        };

      // generates a RFC 3986 encoded, BASE64 encoded HMAC-SHA1 hash
      var encodedSignature = oauthSignature.generate(httpMethod, url, parameters, userConsumerSecret);
      console.log("adarsh encodedSignature "+ encodedSignature);

      return 'OAuth realm="'+realm+'",callback="callback",path="",oauth_consumer_key="'+userConsumerKey+'",oauth_signature_method="HMAC-SHA1",oauth_timestamp="'+timestamp+'",oauth_nonce="'+nonce+'",oauth_version="1.0",oauth_signature="'+encodedSignature+'"';
    }else{
      return '';
    }
  }
};
