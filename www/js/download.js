var downloadApp = {
  fileName: "",
  thumbnailFileName: "",
  
  downloadFile: function(uriString, targetFile, fileType) {

    var complete = function() {
      // Done. Completed successfully
        currently_downloading = false;
        databaseApp.updateYAMLdownloadedRecord(currently_downloading_uuid, downloadApp.fileName);
    };
    var completeThumbnail = function() {
      currently_downloading_thumbnail = false;
      databaseApp.updateYAMLofflineThumbnailRecord(currently_downloading_thumbnail_uuid, downloadApp.thumbnailFileName);
    };
    var error = function (err) {
        console.log('Error: ' + err);
        currently_downloading = false;
    };
    var progress = function(progress) {
      console.log('download progress: ' + (100 * progress.bytesReceived / progress.totalBytesToReceive) + '%');
    };

    try {

        var downloader = new BackgroundTransfer.BackgroundDownloader();
        // Create a new download operation.
        var download = downloader.createDownload(uriString, targetFile);
        // Start the download and persist the promise to be able to cancel the download.
        console.log('111 fileType: ' + fileType);
        if(fileType == "offline_thumbnail"){
          console.log('111 in offline_thumbnail: ' + fileType);
          downloadApp.downloadPromise = download.startAsync().then(completeThumbnail, error, progress);
        }else{
          downloadApp.downloadPromise = download.startAsync().then(complete, error, progress);
        }

    } catch(err) {
        console.log('Error: ' + err);
        currently_downloading = false;
    }
  },
  
  fail : function(error) {
    // https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-file/#list-of-error-codes-and-meanings
    console.log("nik-error fail " + error.code + JSON.stringify(error));
    currently_downloading = false;
  },

  databaseFail : function(tx, error) {
    console.log("nik-error databaseFail " + error.code + error.message);
    currently_downloading = false;
  },

  stopDownload: function () {
      downloadApp.downloadPromise && downloadApp.downloadPromise.cancel();
  },
};