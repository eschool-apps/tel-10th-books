var APPLICATION_VERSION = "1.0.0";

var API_URL = "http://eschool2go.org/api/v1/project/ba7ea038-2e2d-4472-a7c2-5e4dad7744e3?path=";
var AUTH_API_URL = "http://eschool2go.org/api/v2/project/ba7ea038-2e2d-4472-a7c2-5e4dad7744e3?callback=callback&path=";
var CATEGORY_URL = "http://eschool2go.org/api/v1/project/ba7ea038-2e2d-4472-a7c2-5e4dad7744e3?callback=callback";
var PROJECT_UUID = "ba7ea038-2e2d-4472-a7c2-5e4dad7744e3";
var API_DOMAIN = "";
var WEB_API_KEYS_URL = "http://www.eschool2go.org/user";
var WEB_STUDIO_URL = "http://www.eschool2go.org/node/add";

var OFFLINE_EXPLORER_VIEWER_PAGE_ID = "list-offline-explorer-page";
var OFFLINE_EXPLORER_PAGE_NAME = "offline-explorer.html";
var OFFLINE_EXPLORER_LISTVIEW_ID = "offline-contents-list";

var SETTING_LOCAL_STORAGE_NAME = "settings";
var DATABASE_NAME = "eschooltogoSQLitee.db";
var SETTING_PAGE_NAME = "settings.html";
var SETTING_PAGE_SDCARD_LOCATION_TEXT_BOX_ID = "sdcard-loc";
var CLEAR_OFFLINE_DATA_BUTTON_ID = "clearOfflineDataBtn";
var SETTING_PAGE_USER_CONSUMER_KEY_TEXT_BOX_ID = "studio-api-consumerKey";
var SETTING_PAGE_USER_CONSUMER_SECRET_TEXT_BOX_ID = "studio-api-consumerSecret";

var PDF_VIEWER_PAGE_ID = "pdfviewer-page";

var INSTALLATION_CHECK_VALUE = "eschooltogo_installed";
var SETTING_PAGE_ID = "settings-page";
var WEB_VIEWER_PAGE_ID = "webviewer-page";
var EXPLORER_VIEWER_PAGE_ID = "list-explorer-page";
var EXPLORER_LISTVIEW_ID = "contents-list";
var HOME_PAGE_REFRESH_BTN_ID = "refreshButton";
var VIDEO_FOLDER_NAME = "_videos";

var HOME_PAGE_NAME = "index.html";
var EXPLORER_PAGE_NAME = "explorer.html";

var SDCARD_DATABASE_FOLDER_NAME = "_databases";
var APPLICATION_DATABASE_FOLDER_NAME = "databases";
