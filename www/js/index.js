/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var sdcardLoc = '',
  userConsumerKey = '',
  userConsumerSecret = '',
  projectRetrieveAPIurl = API_URL,
  isAuthenticatedUser = false;
var currently_downloading = false;
var currently_downloading_uuid = '';
var currently_downloading_thumbnail = false;
var currently_downloading_thumbnail_uuid = '';
var infinitePollFunctionStatus;
var contentsListView;
var isSharingIntent=false;
loadcount=0; 
var app = {
  // Application Constructor
  initialize : function() {
    this.bindEvents();
  },
  initApplication : function() {
    var settingsFromLocalStorage = window.localStorage.getItem("'"
        + SETTING_LOCAL_STORAGE_NAME + "'");
    if (settingsFromLocalStorage && (settingsFromLocalStorage != null)) {
      settings = JSON.parse(settingsFromLocalStorage);
      if (settings != null) {
        if(settings.sdcardLoc){
          sdcardLoc = settings.sdcardLoc;
        }
        if(settings.userConsumerKey){
          userConsumerKey = settings.userConsumerKey;
        }
        if(settings.userConsumerSecret){
          userConsumerSecret = settings.userConsumerSecret;
        }
      }
    }
    if(userConsumerKey && userConsumerSecret){
      console.log("isAuthenticatedUser = true"+userConsumerKey);
      projectRetrieveAPIurl = AUTH_API_URL;
      isAuthenticatedUser = true;
    }

    $(document).on('pageinit', '#' + SETTING_PAGE_ID, function() {
      app.initSettingsPage();
    }).on('pageinit', '#webviewer-page', function() {
      var url = this.getAttribute('data-url').replace(/(.*?)link=/g, '');
      
      app.initWebViewerPage(url);
    }).on('pageinit', '#pdfviewer-page', function() {
      var url = this.getAttribute('data-url').replace(/(.*?)link=/g, '');
      app.initPDFViewerPage(url);
    }).on(
        'pageinit',
        '#list-explorer-page',
        function() {
              //var categParent1 = this.getAttribute('data-url').replace(/(.*?)parent=/g, '');
              var categParent = app.getParameterByName("parent",this.getAttribute('data-url'));
                app.initListExplorerPage(categParent,projectRetrieveAPIurl);
              
              
              
        }).on(
        'pageinit',
        '#' + OFFLINE_EXPLORER_VIEWER_PAGE_ID,
        function() {
          var categParent = this.getAttribute('data-url').replace(
              /(.*?)parent=/g, '');
          if (categParent.indexOf(OFFLINE_EXPLORER_PAGE_NAME) !== -1) {
            categParent = '';
          }
          app.initListOfflineExplorerPage(categParent);
    });
      /*var sharedText = sessionStorage.sharedText;
      if(sharedText){
        app.initCategoryExplorer("",CATEGORY_URL,sharedText);
      }
      else{
        
      }*/
      app.initListExplorerPage('',projectRetrieveAPIurl);
      
    
    /*
     * Create tables, if not already created
     */
    databaseApp.initDatabase();
    databaseApp.initDatabaseTables();

    //infinitePollFunctionStatus = setInterval(app.infinitePeriodicRunner, 4000);
    // app.openLinksInApp();
  },
  getParameterByName : function(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  },

  infinitePeriodicRunner : function() {
    console.log("in infinitePeriodicRunner");
    app.downloadPendingContent();
  },
  downloadPendingContent : function() {
    console.log("in downloadPendingContent currently_downloading "+currently_downloading);
    /* check if variable currently_downloading is false, then proceed
     * select 1 from cache_yaml_files where downloaded = 0 & download_attempts < 3, sorted by id asc
     */
    if(!currently_downloading){
      // TODO: Check if connected to internet, proceed only when connected.
      currently_downloading = true;
      console.log("in downloadPendingContent currently_downloading set to true "+currently_downloading);
      try {
        databaseApp.db
            .transaction(function(transaction) {
              transaction
                  .executeSql(
                      "SELECT cache_yaml_files.* FROM cache_yaml_files WHERE downloaded = 0 AND download_attempts < 3 AND download_url != '' ORDER BY id ASC LIMIT 1",
                      [],
                      function(tx, results) {
                        console.log("results.rows.length "+results.rows.length);
                        if(results.rows.length > 0){
                          var download_url = results.rows.item(0).download_url;
                          console.log("aaa results.rows.item(0).extension %%"+results.rows.item(0).extension+"%%");
                          downloadApp.fileName = results.rows.item(0).display_name + "." + results.rows.item(0).extension;

                          console.log("aaa download_url %%"+download_url+"%%");
                          currently_downloading_uuid = results.rows.item(0).uuid;
                          
                          var thumbnail_url = results.rows.item(0).thumbnail_url;
                          console.log("aaa thumbnail_url %%"+thumbnail_url+"%%");
                          if(thumbnail_url && !currently_downloading_thumbnail){
                            downloadApp.thumbnailFileName = results.rows.item(0).display_name + "." + thumbnail_url.substr(thumbnail_url.lastIndexOf('.') + 1);
                            console.log("aaa downloadApp.thumbnailFileName %%"+downloadApp.thumbnailFileName+"%%");
                            currently_downloading_thumbnail_uuid = currently_downloading_uuid;
                            currently_downloading_thumbnail = true;
                          }
                          
                          console.log("aaa download_attempts "+results.rows.item(0).download_attempts);
                          databaseApp.updateYAMLdownloadAttemptsRecord(currently_downloading_uuid, results.rows.item(0).download_attempts);
                          console.log("bbb sdcardLoc "+sdcardLoc);
                          console.log("bbb downloadApp.fileName "+downloadApp.fileName);
                          window.resolveLocalFileSystemURL("file:///" + sdcardLoc, function(dir) {
                            console.log("got main dir" + JSON.stringify(dir));
                            /*
                             * Check if sub directories exist, If not create recursively
                             */
                            var printDirPath = function(entry){
                              console.log("Dir path - " + entry.fullPath + " downloadApp.fileName ^^" + downloadApp.fileName+"^^");
                              entry.getFile(downloadApp.fileName, {create:true}, function(file) {
                                console.log("got the offline_file file " + JSON.stringify(file));
                                downloadApp.downloadFile(download_url, file, "offline_file");
                              }, downloadApp.fail);
                              console.log("111 if thumbnail_url - " + thumbnail_url + " currently_downloading_thumbnail ^^" + currently_downloading_thumbnail +"^^");
                              if(true || thumbnail_url && !currently_downloading_thumbnail){
                                console.log("111 getting thumbnail");
                                entry.getFile(downloadApp.thumbnailFileName, {create:true}, function(file) {
                                  console.log("111 got the thumbnailFileName file " + JSON.stringify(file));
                                  downloadApp.downloadFile(thumbnail_url, file, "offline_thumbnail");
                                }, downloadApp.fail);
                              }
                            }
                            console.log("bbb createDirectoryRecusively "+results.rows.item(0).path);
                            AppFile.createDirectoryRecusively(dir, results.rows.item(0).path, printDirPath);
                            /*dir.getFile(file_name, {create:true}, function(file) {
                                console.log("got the file " + JSON.stringify(file));
                                downloadApp.downloadFile(download_url, file);
                            });*/
                          }, downloadApp.fail);
                        }else{
                          // No results - null handler
                          console.log("SQLite no results: ");
                          currently_downloading = false;
                        }
                    }, downloadApp.databaseFail );
              }, downloadApp.databaseFail);
        } catch (err) {
          console.log(err);
          currently_downloading = false;
          return false;
        }
    }
  },
  // Bind Event Listeners
  //
  // Bind any events that are required on startup. Common events are:
  // 'load', 'deviceready', 'offline', and 'online'.
  bindEvents : function() {
    document.addEventListener('deviceready', this.onDeviceReady, false);
    document.addEventListener('deviceready',function(){
      // using previously determined OS
             window.plugins.intent.getCordovaIntent( function( Intent )
            {
              // when starting the app from a cold start (ie NOT from another app passing data),
              // the "current" intent action is MAIN.  Ignore it.
                app.getIntentInfo(Intent);
              
            })
            window.plugins.intent.setNewIntentHandler( function( Intent )
            {
              // when our app is brought to the foreground (ie it is currently running) by
              // another app passing a CSV file, this code is called.
              // android.intent.action.MAIN IS called when app brought to foreground so need to limit
              // the import when NOT MAIN intent (Updated this Jan 17 2017)
                app.getIntentInfo(Intent);
            })
    },false);

    document.addEventListener('deviceready',function(){


    },false);
  },
  // deviceready Event Handler
  //
  // The scope of 'this' is the event. In order to call the 'receivedEvent'
  // function, we must explicitly call 'app.receivedEvent(...);'
  onDeviceReady : function() {
        //app.receivedEvent('deviceready');
        app.initApplication();
  },
  initSettingsPage : function() {
    if (sdcardLoc) {
      console.log("setting sdcardLoc "+sdcardLoc);
      $("#" + SETTING_PAGE_SDCARD_LOCATION_TEXT_BOX_ID).val(sdcardLoc);
    }
    if (userConsumerKey) {
      $("#" + SETTING_PAGE_USER_CONSUMER_KEY_TEXT_BOX_ID).val(userConsumerKey);
    }
    if (userConsumerSecret) {
      $("#" + SETTING_PAGE_USER_CONSUMER_SECRET_TEXT_BOX_ID).val(userConsumerSecret);
    }

    $("#btnFolderBrowser").click(
        function() {
          console.log("in btnFolderBrowser");
          window.OurCodeWorld.Filebrowser.folderPicker.single({
            success : function(data) {
              console.log(JSON.stringify(data));
              if (!data.length) {
                return;
              }
              $("#" + SETTING_PAGE_SDCARD_LOCATION_TEXT_BOX_ID).val(
                  data[0].replace("file:///", "") + "/");
              // Array with paths
              // ["file:///storage/emulated/0/360/security",
              // "file:///storage/emulated/0/360/security"]
            },
            error : function(err) {
              console.log(JSON.stringify(err));
              console.log(err);
            }
          });
          console.log("after folderPicker");
        });
    $("#btnLoginAPI").click(
        function() {
          app.openInAppBrowser(WEB_API_KEYS_URL);
          //var ref = cordova.InAppBrowser.open(WEB_API_KEYS_URL, '_blank', 'location=yes');
          /*SafariViewController.isAvailable(function (available) {
            if (available) {
              SafariViewController.show({
                    url: WEB_API_KEYS_URL,
                    hidden: false, // default false. You can use this to load cookies etc in the background (see issue #1 for details).
                    animated: true, // default true, note that 'hide' will reuse this preference (the 'Done' button will always animate though)
                    transition: 'curl', // (this only works in iOS 9.1/9.2 and lower) unless animated is false you can choose from: curl, flip, fade, slide (default)
                    enterReaderModeIfAvailable: false, // default false
                    tintColor: "#00ffff", // default is ios blue
                    barColor: "#0000ff", // on iOS 10+ you can change the background color as well
                    controlTintColor: "#ffffff" // on iOS 10+ you can override the default tintColor
                  },
                  // this success handler will be invoked for the lifecycle events 'opened', 'loaded' and 'closed'
                  function(result) {
                    if (result.event === 'opened') {
                      console.log('opened');
                    } else if (result.event === 'loaded') {
                      console.log('loaded');
                    } else if (result.event === 'closed') {
                      console.log('closed');
                    }
                  },
                  function(msg) {
                    console.log("KO: " + msg);
                  })
            } else {
              // potentially powered by InAppBrowser because that (currently) clobbers window.open
              //window.open(url, '_blank', 'location=yes');
              var ref = cordova.InAppBrowser.open(WEB_API_KEYS_URL, '_blank', 'location=yes');
            }
          });*/
        });

    $("#copyDBtoSDcard").click(
        function() {
          // console.log("droidbase: in copyDBtoSDcard");
          // /data/data/com.technikh.eschooltogo/databases
          // console.log("droidbase:
          // "+cordova.file.applicationStorageDirectory); //
          // file:///data/data/com.technikh.eschooltogo/
          window.resolveLocalFileSystemURL(
              cordova.file.applicationStorageDirectory
                  + APPLICATION_DATABASE_FOLDER_NAME + "/" + DATABASE_NAME,
              AppFile.copyDBtoSDcard, app.fail);
        });
    $("#copyDBfromSDcard").click(
        function() {
          // console.log("droidbase: in copyDBfromSDcard");
          var settings = JSON.parse(window.localStorage.getItem("'"
              + SETTING_LOCAL_STORAGE_NAME + "'"));
          sdcardLoc = settings.sdcardLoc;
          // console.log("nik-success sdcardLoc"+sdcardLoc);
          window.resolveLocalFileSystemURL("file:///" + sdcardLoc
              + SDCARD_DATABASE_FOLDER_NAME + "/" + DATABASE_NAME,
              AppFile.copyDBfromSDcard, app.fail);
        });
    $("#uploadAnalyticsToServer").click(
        function() {
          // console.log("droidbase: in uploadAnalyticsToServer");
          analyticsApp.uploadBatchToServer();
        });
    $("#clearCacheBtn").click(
        function() {
          // console.log("droidsung: in clearCacheBtn nik-" +
          // cordova.file.externalRootDirectory);
          var settings = JSON.parse(window.localStorage.getItem("'"
              + SETTING_LOCAL_STORAGE_NAME + "'"));
          sdcardLoc = settings.sdcardLoc;

          // console.log("droidsung: before openDatabase");
          app.db = window.sqlitePlugin.openDatabase({
            name : DATABASE_NAME,
            location : 'default'
          });
          // console.log("droidsung: after openDatabase");
          app.prepareCacheTables();

          window.resolveLocalFileSystemURL("file:///" + sdcardLoc,
              app.addFileEntry, app.fail);
          window.localStorage.setItem(INSTALLATION_CHECK_VALUE, true);
        });
    $("#"+CLEAR_OFFLINE_DATA_BUTTON_ID).click(
        function() {
          console.log("CLEAR_OFFLINE_DATA_BUTTON_ID");
          databaseApp.dropDatabaseDownloadsTable();
          databaseApp.createDatabaseDownloadsTable();
          navigator.notification.alert('Offline data removed.');
        });
    $('#settings-form').submit(
        function(event) {
          event.preventDefault();
          sdcardLoc = $("#" + SETTING_PAGE_SDCARD_LOCATION_TEXT_BOX_ID).val()
              .trim();
          userConsumerKey = $("#" + SETTING_PAGE_USER_CONSUMER_KEY_TEXT_BOX_ID).val()
            .trim();
          userConsumerSecret = $("#" + SETTING_PAGE_USER_CONSUMER_SECRET_TEXT_BOX_ID).val()
            .trim();
          var settings = {
            "sdcardLoc" : sdcardLoc,
            "userConsumerKey" : userConsumerKey,
            "userConsumerSecret" : userConsumerSecret
          };
          window.localStorage.setItem("'" + SETTING_LOCAL_STORAGE_NAME + "'",
              JSON.stringify(settings));
          navigator.notification.alert('Settings saved.');
        });
  },
  initWebViewerPage : function(url) {
    //console.log("url " + url);
    app.openInAppBrowser(url);
    //var ref = cordova.InAppBrowser.open(url, '_blank', 'location=yes');
    //$('#web-view-frame').attr("src", url);
  },
  initPDFViewerPage : function(url) {
    console.log("initPDFViewerPage url " + url);
    $('#pdf-view-frame').attr("src", "js/pdfjs/web/viewer.html?file="+url);
    /*
    if (!PDFJS.PDFViewer || !PDFJS.getDocument) {
      console.log('Please build the pdfjs-dist library using\n'
          + '  `gulp dist`');
    }
    // The workerSrc property shall be specified.
    PDFJS.workerSrc = 'js/pdfjs/build/pdf.worker.js';

    // var DEFAULT_URL = '../../web/compressed.tracemonkey-pldi-09.pdf';
    //var DEFAULT_URL = 'http://172.27.81.10:3000/js/chapter_1.pdf';
    var DEFAULT_URL = url;
    var SEARCH_FOR = ''; // try 'Mozilla';

    var container = document.getElementById('viewerContainer');

    // (Optionally) enable hyperlinks within PDF files.
    var pdfLinkService = new PDFJS.PDFLinkService();

    var pdfViewer = new PDFJS.PDFViewer({
      container : container,
      linkService : pdfLinkService,
    });
    pdfLinkService.setViewer(pdfViewer);

    // (Optionally) enable find controller.
    var pdfFindController = new PDFJS.PDFFindController({
      pdfViewer : pdfViewer
    });
    pdfViewer.setFindController(pdfFindController);

    container.addEventListener('pagesinit', function() {
      // We can use pdfViewer now, e.g. let's change default scale.
      pdfViewer.currentScaleValue = 'page-width';

      if (SEARCH_FOR) { // We can try search for things
        pdfFindController.executeCommand('find', {
          query : SEARCH_FOR
        });
      }
    });

    // Loading document.
    PDFJS
        .getDocument(DEFAULT_URL)
        .then(
            function(pdfDocument) {
              // Document loaded, specifying document for the viewer and
              // the (optional) linkService.
              pdfViewer.setDocument(pdfDocument);

              pdfLinkService.setDocument(pdfDocument, null);

              setTimeout(
                  function() {
                    //console.log("setTimeout on load");
                    $('div.textLayer div')
                        .each(
                            function() {
                              var contentfull = $(this).html();
                              // console.log(contentfull);
                              var newcontent = contentfull
                                  .replace(/the/g,
                                      '<a style="background:red; margin-left: -2px;" href="#">the </a>');
                              //console.log(newcontent);
                              $(this).html(newcontent);
                            });

                  }, 4000);
            });*/
  },
  initListOfflineExplorerPage : function(categParent) {
    //console.log("in initListOfflineExplorerPage categParent "+categParent);
    databaseApp.db = window.sqlitePlugin.openDatabase({
      name : DATABASE_NAME,
      location : 'default'
    });
    var hyphened_categParent = categParent.replace(/\//g, '-');
    hyphened_categParent = hyphened_categParent.replace(/\s+/g, '-');
    console.log("in initListOfflineExplorerPage hyphened_categParent "+hyphened_categParent);
    $('#' + OFFLINE_EXPLORER_LISTVIEW_ID).attr("id", OFFLINE_EXPLORER_LISTVIEW_ID + "-" + hyphened_categParent);
    var $contentsList = $('#' + OFFLINE_EXPLORER_LISTVIEW_ID + "-" + hyphened_categParent);
    try {
      databaseApp.db
          .transaction(function(transaction) {
            transaction
                .executeSql(
                    "SELECT cache_yaml_files.* FROM cache_yaml_files WHERE downloaded = 1 AND offline_file != '' AND cache_yaml_files.path LIKE '"
                    + categParent + "%'",
                    [],
                    function(tx, results) {
                      //console.log("categParent: "+categParent);
                      if (categParent) {
                        categParent = categParent + '/';
                      }
                      console.log("aaa categParent: "+categParent);
                      //console.log(JSON.stringify(results.rows));
                      var len = results.rows.length, i;
                      //console.log("length: "+len);
                      var listItemsObj = {}
                      for (i = 0; i < len; i++) {
                        var filePathWithOutExt = results.rows.item(i).path + "/" + results.rows.item(i).display_name;
                        console.log("aaa filePathWithOutExt "+filePathWithOutExt);
                        console.log("aaa results.rows.item(i).offline_file "+results.rows.item(i).offline_file);
                        
                        /*var filePathWithExt = filePathWithOutExt + "."
                            + results.rows.item(i).extension;*/
                        var filePathArray = filePathWithOutExt.split("/");
                        //console.log(JSON.stringify(filePathArray));
                        var categParentNumOfSlashes = (categParent.split("/").length-1);
                        var filePathNumOfSlashes = (filePathArray.length-1);
                        console.log("aaa categParentNumOfSlashes "+categParentNumOfSlashes+" filePathNumOfSlashes "+filePathNumOfSlashes);
                        // First component of a path like English/Khan Academy/Biology
                        var listItemName = filePathArray[categParentNumOfSlashes];
                        console.log("aaa listItemName "+listItemName);
                        if (filePathNumOfSlashes == (categParentNumOfSlashes)) {
                          listItemsObj[listItemName] = {
                            "isFile" : true,
                            "uuid" : results.rows.item(i).uuid,
                            "ext" : results.rows.item(i).extension,
                            "type" : results.rows.item(i).type,
                            "offline_file" : results.rows.item(i).offline_file,
                            "image" : results.rows.item(i).offline_thumbnail,
                            "name" : results.rows.item(i).display_name
                          };
                        } else {
                          listItemsObj[listItemName] = {
                            "isFile" : false,
                            "name" : listItemName
                          };
                        }
                      }
                      console.log("aaa 1 "+JSON.stringify(listItemsObj[Object.keys(listItemsObj)[0]]));
                      // If length of listItemsObj is 1 and not file
                      if (Object.keys(listItemsObj).length == 1
                          && listItemsObj[Object.keys(listItemsObj)[0]].isFile == false) {
                        console.log("aaa  single folder found. Analyzing to merge paths.");
                        var folderMultiPathArray = [];
                        var minimum_length = 100;
                        for (i = 0; i < len; i++) {
                          // English/Textbooks/India/Telangana/SSC/Biology/2.
                          // Respiration -
                          // English/Textbooks/India/Telangana/SSC/Biology/1.
                          // Nutrition -
                          // English/Textbooks/India/Telangana/SSC/Physics/1.
                          // Heat -
                          // categParent: English/Textbooks
                          var filePathWithOutExt = results.rows.item(i).path + "/" + results.rows.item(i).name;
                          // Remove categParent
                          var folderPath = filePathWithOutExt
                              .substring(categParent.length);
                          // Remove file name
                          var folderPathArray = folderPath.split("/");
                          folderPathArray.pop()
                          folderMultiPathArray.push(folderPathArray);
                          if (folderPathArray.length < minimum_length) {
                            minimum_length = folderPathArray.length;
                          }
                        }
                        var matchingComponentArray = [];
                        for (i = 0; i < minimum_length; i++) {
                          var matching_component = folderMultiPathArray[0][i];
                          var is_matched = true;
                          for (var j = 1; j < folderMultiPathArray.length; j++) {
                            if (folderMultiPathArray[j][i] != matching_component) {
                              is_matched = false;
                            }
                          }
                          if (is_matched == true) {
                            matchingComponentArray.push(matching_component);
                          }
                        }
                        var listItemsObj = {}
                        listItemsObj[matchingComponentArray.join('/')] = {
                          "isFile" : false,
                          "name" : matchingComponentArray.join(' -> ')
                        };
                      }
                      console.log("aaa "+JSON.stringify(listItemsObj));
                      $
                          .each(
                              listItemsObj,
                              function(i, el) {
                                console.log(JSON.stringify(el));
                                if (el.isFile == true) {
                                  var file_display_name = i;
                                  if (el.name) {
                                    file_display_name = el.name;
                                  }
                                  var image_html = '';
                                  if (el.image) {
                                    image_html = '<img height="70px" style="padding: 5px;" src="file:///'
                                        + sdcardLoc
                                        + categParent
                                        + el.image
                                        + '" />';
                                  }
                                  console.log("aaa image_html "+image_html);
                                  if (el.type && (el.type == "video")) {
                                    // check for video file webm or mp4 or mkv
                                    // _videos folder
                                    // replace space with %20
                                    // http://android.stackexchange.com/questions/4775/how-can-i-open-an-html-file-i-have-copied-from-pc-to-sd-card-of-phone
                                    // %20 or whole string in quotes
                                    // "/mnt/sdcard/Documents/To Read.html"
                                    console.log("before opener_file_url sdcardLoc "+sdcardLoc);
                                    /*var opener_file_url = sdcardLoc
                                        + '_videos/' + categParent
                                        + el.offline_file.trim();*/
                                    var opener_file_url = sdcardLoc
                                    + '_videos/' + categParent;
                                    console.log("after opener_file_url"+opener_file_url);
                                    opener_file_url = encodeURIComponent(opener_file_url);
                                    // opener_file_url =
                                    // opener_file_url.replace(/\s+/g, '%20');
                                    var htmlItems = '<li>'
                                        + image_html
                                        + '<span onclick="app.onClickOfFileOpener(\''
                                        + opener_file_url + '\')">'
                                        + file_display_name + '</span></li>';
                                  } else if (el.type
                                      && (el.type == "article" || el.type == "definition")) {
                                    // check for video file webm or mp4 or mkv
                                    // _videos folder
                                    // replace space with %20
                                    // http://android.stackexchange.com/questions/4775/how-can-i-open-an-html-file-i-have-copied-from-pc-to-sd-card-of-phone
                                    // %20 or whole string in quotes
                                    // "/mnt/sdcard/Documents/To Read.html"
                                    var opener_file_url = sdcardLoc
                                        + categParent
                                        + el.offline_file.trim();
                                    //opener_file_url = opener_file_url.replace(/\s+/g, '%20');
                                    var htmlItems = '<li><a href="pdfviewer.html?uuid='
                                        + el.uuid
                                        + '&link=file:///'
                                        + opener_file_url
                                        + '">'
                                        + image_html + file_display_name + '</a></li>';
                                  } else {
                                    // unknown file type
                                    var htmlItems = '<li>'
                                        + image_html
                                        + '<span onclick="window.plugins.fileOpener.open(\'file:///'
                                        + sdcardLoc + categParent + i
                                        + '\')">' + file_display_name
                                        + '</span></li>';
                                  }
                                } else {
                                  var htmlItems = '<li><a href="offline-explorer.html?parent='
                                      + categParent
                                      + i
                                      + '">'
                                      + el.name
                                      + '</a></li>';
                                }
                                console.log(htmlItems);
                                $contentsList.append(htmlItems);
                              });
                      console.log("after each");
                      $contentsList.listview('refresh');
                      $.mobile.loading('hide');
                    }, null);
          });
    } catch (err) {
      console.log(err);
      if (navigator.notification) {
        navigator.notification.alert('Error: ' + err);
      }
      return false;
    }
  },
  // Download Categories JSON API
  initListExplorerPage : function(categParent,api_url) {
    if(!isSharingIntent){
    contentsListView=$('#contents-list');
    var category_ajax_url = api_url;
    var ajax_data = {};
    if(isAuthenticatedUser){
      $.ajaxSetup({
        cache : false
      });
      category_ajax_url = "http://eschool2go.org/sites/all/modules/custom-features/analytics_service/proxy-oauth-get.php";
      ajax_data = { 'apiURL': api_url, 'userConsumerKey': userConsumerKey, 'userConsumerSecret': userConsumerSecret };
    }
    console.log("test " + category_ajax_url);
    /*console.log("userConsumerKey " + userConsumerKey);
    console.log("userConsumerSecret " + userConsumerSecret);
    console.log("type: " + (isAuthenticatedUser == true) ? 'POST' : 'GET');
    console.log(ajax_data);*/

    //var oauth_auth_header = apiServicesApp.make_oauth_auth_header(api_url, 'GET', userConsumerKey, userConsumerSecret);
    $.ajax({
          url : category_ajax_url,
          dataType : 'JSON',
          //jsonpCallback : 'callback',
          type : (isAuthenticatedUser == true) ? 'POST' : 'GET',
          data: ajax_data,
          //contentType : "application/json; charset=utf-8",
          error : function (xhr, ajaxOptions, thrownError) {
            //console.log(xhr.status);
            console.log(thrownError);
          },
          success : function(categoriesObj) {
            console.log("success categoriesObj");
            console.log(JSON.stringify(categoriesObj));
            /*
             * var decoded = $('<div/>').html(result).text(); categoriesObj =
             * JSON.parse(decoded);
             */

            // Uncaught TypeError: Cannot read property 'replace' of null
            var hyphened_categParent = '';
            if (categParent) {
              // Replace / with space
              hyphened_categParent = categParent.replace(/\//g, ' ');
              // Replace dots with space
              hyphened_categParent = hyphened_categParent.replace(/\./g,' ');
              // Replace spaces with - http://stackoverflow.com/questions/7764319/how-to-remove-duplicate-white-spaces-in-a-string
              hyphened_categParent = hyphened_categParent.replace(/\s+/g, '-');
            }
            contentsListView.attr("id",
                "contents-list-" + hyphened_categParent);
            //console.log('#contents-list-' + hyphened_categParent);
            contentsListView = $('#contents-list-' + hyphened_categParent);
            //console.log(contentsListView.attr('id'));
            if (categParent) {
              var categParentArray = categParent.split("/");
              $.each(categParentArray, function(i, el) {
                categoriesObj = categoriesObj[el]["items"];
              });
            }

            if (categoriesObj) {
              $
                  .each(
                      categoriesObj,
                      function(i, el) {
                        var parentPath = i;
                        if (categParent) {
                          parentPath = categParent + "/" + i;
                        }
                        var image_html = '';
                        if (el.thumbnails) {
                          image_html = '<img height="70px" style="padding: 5px;" src="'+API_DOMAIN+ el.thumbnails[0] + '" />';
                        }
                        var htmlItems = '<li><a href="index.html?parent='
                            + parentPath + '">' + image_html + i + '</a></li>';
                        contentsListView.append(htmlItems);
                      });
            }
            contentsListView.listview('refresh');
            if (categParent) {
              // var api_url =
              // "http://opencurricula.technikh.com/api/v1/categories/English/Wikipedia/Mathematics/Discrete%20mathematics/Coding%20theory/";
              var content_ajax_url = projectRetrieveAPIurl + categParent;
              if(isAuthenticatedUser){
                content_ajax_url = "http://eschool2go.org/sites/all/modules/custom-features/analytics_service/proxy-oauth-get.php";
                ajax_data = { 'apiURL': encodeURI(projectRetrieveAPIurl + categParent), 'userConsumerKey': userConsumerKey, 'userConsumerSecret': userConsumerSecret };
              }
              console.log(content_ajax_url);
              console.log(encodeURI(content_ajax_url));
              console.log(encodeURI(projectRetrieveAPIurl + categParent));
              $.ajax({
                url : encodeURI(content_ajax_url),
                dataType : 'JSON',
                //jsonpCallback : 'callback',
                type : (isAuthenticatedUser == true) ? 'POST' : 'GET',
                data: ajax_data,
                //contentType : "application/json; charset=utf-8",
                error : function (xhr, ajaxOptions, thrownError) {
                  //console.log(xhr.status);
                  console.log(thrownError);
                },
                success : function(pagesObj) {
                  console.log("success content_ajax_url pagesObj");
                  console.log(JSON.stringify(pagesObj));
                  if (pagesObj !== null){
                    /*
                     * var decoded = $('<div/>').html(result).text();
                     * categoriesObj = JSON.parse(decoded);
                     */
                    $.each(pagesObj, function(i, el) {
                      console.log(i);
                      var htmlItems = app.onlineFileLaunchingMarkup(el);
                      console.log(htmlItems);
                      contentsListView.append(htmlItems);
                    });
                    contentsListView.listview('refresh');
                  }
                }
              });
            }
          }
        });
    console.log("After Ajax");
      }
  },
  onClickOfFileOpener : function(opener_file_url) {
    //console.log( "jennik: onClickOfFileOpener:" + opener_file_url );
    // category, action, location, label, value
    //analyticsApp.onAnalyticsEvent('Explorer', 'file click', opener_file_url, '', '');
    window.plugins.fileOpener.open('file:///' + opener_file_url);
  },
  
  /* offlineExplorer.markFileforDownloading
   * id, uuid, path, doc.type, download_url, doc.offline_file, thumbnail_url, offline_thumbnail, doc.title, description, downloaded(0/1)
   * From API: uuid, path, doc.type, download_url, thumbnail_url, title, description
   * API pending: uuid, category_path, download_url
   * first thumbnail_url from thumbnail_urls array
   * API construct: https://github.com/TechNikh/SculpinPathNavigationBundle/blob/master/PathNavigationProvider.php#L57
   * http://opencurricula.technikh.com/api/v1/categories/English/Videos/Biology/Animations/?callback=callback&_=1487864893731
   * {  
title:"1.1 Developing Egg Cells",
url:"open-curricula-files/_videos/English/Videos/Biology/Animations/1.1 Developing Egg Cells.md",
uuid:"df928d59-04f6-4ce8-b7ad-ac0eb07e4a88",
updated:"1486069676",
type:"video",
id:"rZLthn_3UEc",
download_url:"https://s3.amazonaws.com/eschool2go-offline/_videos/English/Videos/Biology/Animations/1.1%20Developing%20Egg%20Cells.webm",
category_path:"English/Videos/Biology/Animations",
content:"",
tags:"",
thumbnails:[  
"https://i3.ytimg.com/vi/rZLthn_3UEc/default.jpg",
"https://i3.ytimg.com/vi/rZLthn_3UEc/1.jpg",
"https://i3.ytimg.com/vi/rZLthn_3UEc/2.jpg",
"https://i3.ytimg.com/vi/rZLthn_3UEc/3.jpg"
],
provider:"YouTube"
},
   */
  onlineFileLaunchingMarkup : function(el) {
    var image_html = '', 
        offlineDownloadHTML = '';
    if (el.thumbnails) {
      image_html = '<img height="70px" style="padding: 5px;" src="'
          + el.thumbnails[0] + '" />';
    }
    // TODO: markFileforDownloading markup only if el.download_url exists
    if (el.download_url) {
      offlineDownloadHTML = '<a onclick="offlineExplorer.markFileforDownloading(\''+el.uuid+'\', \''+el.category_path+'\')" href="#" data-icon="arrow-d" >Download</a>';
    }
    var htmlItems = '';
    switch (el.type) {
      case 'article':
        if (el.id.endsWith(".pdf")) {
          // Can't open through native browser because of PDF integration with offline anootations...
          var htmlItems = '<li><a href="pdfviewer.html?link='
              + el.id + '">' + image_html + el.title
              + '</a>'+offlineDownloadHTML+'</li>';

        } else {
          var htmlItems = '<li><span onclick="app.openInAppBrowser(\''
              + el.id + '\')">' + image_html + el.title
              + '</span>'+offlineDownloadHTML+'</li>';
        }
        break;
      case 'h5p_content':
        var htmlItems = '<li><span onclick="app.openInAppBrowser(\''
          + el.id + '\')">' + image_html + el.title
          + '</span>'+offlineDownloadHTML+'</li>';
        break;
      default:
    }
    return htmlItems;
  },
  openInAppBrowser : function(url) {
    console.log(url);
    //var ref = cordova.InAppBrowser.open(url, '_blank', 'location=yes');
    SafariViewController.isAvailable(function (available) {
      if (available) {
        SafariViewController.show({
              url: url,
              hidden: false, // default false. You can use this to load cookies etc in the background (see issue #1 for details).
              animated: true, // default true, note that 'hide' will reuse this preference (the 'Done' button will always animate though)
              transition: 'curl', // (this only works in iOS 9.1/9.2 and lower) unless animated is false you can choose from: curl, flip, fade, slide (default)
              enterReaderModeIfAvailable: false, // default false
              tintColor: "#00ffff", // default is ios blue
              barColor: "#0000ff", // on iOS 10+ you can change the background color as well
              controlTintColor: "#ffffff" // on iOS 10+ you can override the default tintColor
            },
            // this success handler will be invoked for the lifecycle events 'opened', 'loaded' and 'closed'
            function(result) {
              if (result.event === 'opened') {
                console.log('opened');
              } else if (result.event === 'loaded') {
                console.log('loaded');
              } else if (result.event === 'closed') {
                console.log('closed');
              }
            },
            function(msg) {
              console.log("KO: " + msg);
            })
      } else {
        // potentially powered by InAppBrowser because that (currently) clobbers window.open
        //window.open(url, '_blank', 'location=yes');
        var ref = cordova.InAppBrowser.open(url, '_blank', 'location=yes');
      }
    });
  },
  // Download Categories JSON API
  initCategoryExplorer : function(categParent,api_url,sharedText, sharedSubject) {
     
       $('h1').text("Select your category");
       contentsListView=$('#main-content ul:first');
       $( "button" ).remove();
       $("footer").hide();
       //alert(categParent);
       $( "<button id='savebutton' type=\"button\" >Save here</button>  <button id='addcategorybutton' type=\"button\">Add Category</button>  " ).insertAfter( "#main-content" );
       
          $("#savebutton").click(function(){
              //alert("SaveText:"+sharedText+" at location:"+categParent);
           
              app.openForm(categParent,sharedText, sharedSubject);
              $('h1').text("10th Class Study material");
              contentsListView.empty();
              $( "button" ).remove();
              contentsListView.attr("id","contents-list");
              isSharingIntent=false;
               $("footer").show();
              app.initListExplorerPage("",projectRetrieveAPIurl);
             
          });
          $("#addcategorybutton").click(function(){
              var text = prompt("Input the new category", "");
              // TODO: handle cancel action on prompt
              if(text!=""){
                  //Call add category API here and uncomment the line below to show in the listview
                  apiServicesApp.createCategory(categParent, text);
                  alert(categParent+"/"+text);
                  app.initCategoryExplorer(categParent,api_url,sharedText, sharedSubject);
              }
              else{
                  alert("Please you cant leave the category text blank");
              }
                
          });
      
    
          var category_ajax_url = api_url;
          var ajax_data = {};
          if(isAuthenticatedUser){
            $.ajaxSetup({
              cache : false
            });
            category_ajax_url = "http://eschool2go.org/sites/all/modules/custom-features/analytics_service/proxy-oauth-get.php";
            ajax_data = { 'apiURL': api_url, 'userConsumerKey': userConsumerKey, 'userConsumerSecret': userConsumerSecret };
          }
          console.log("test " + category_ajax_url);
     console.log("test initCategoryExplorer " + api_url);
    $.ajax({
          url : category_ajax_url,
          dataType : 'JSON',
          //jsonpCallback : 'callback',
          type : (isAuthenticatedUser == true) ? 'POST' : 'GET',
          data: ajax_data,
          error : function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
          },
          success : function(categoriesObj) {
             console.log(categoriesObj);
            /*
             * var decoded = $('<div/>').html(result).text(); categoriesObj =
             * JSON.parse(decoded);
             */
            
            var hyphened_categParent = categParent.replace(/\//g, '-');
            console.log(hyphened_categParent);
            // Replace spaces with -
            hyphened_categParent = hyphened_categParent.replace(/\s+/g, '-');
            contentsListView.attr("id","contents-list-" + hyphened_categParent);
            contentsListView = $('#contents-list-' + hyphened_categParent);
            contentsListView.empty();
            
            if (categParent) {
              var categParentArray = categParent.split("/");
              $.each(categParentArray, function(i, el) {
                categoriesObj = categoriesObj[el]["items"];
                console.log(categoriesObj);
              });
            }
            
            if (categoriesObj) {
              $
                  .each(
                      categoriesObj,
                      function(i, el) {
                        var parentPath = i;
                        if (categParent) {
                          parentPath = categParent + "/" + i;
                        }
                        var image_html = '';
                        var htmlItems = '<li><a class="clickablecategory" data-val="'
                            + parentPath + '">' + image_html + i + '</a></li>'
                        //alert(htmlItems);
                        //console.log(htmlItems);
                        contentsListView.append(htmlItems);
                      });
            }
            contentsListView.listview('refresh');
           
            app.getChildCategory(sharedText, sharedSubject);
            
            
          }
        });
  },
  getChildCategory : function(sharedText, sharedSubject){
    $('.clickablecategory').click(function(){
              //alert($(this).data("val").toString());
              //alert("aaya");
              
              var str="";
              str= $(this).data("val").toString();
              app.initCategoryExplorer(str,CATEGORY_URL,sharedText, sharedSubject);
      });
  },
  getIntentInfo : function(Intent){
    if ( Intent.action != 'android.intent.action.MAIN' )
    {
      if(isAuthenticatedUser){
        var networkState = navigator.connection.type;
        if(networkState!=Connection.NONE){
            isSharingIntent=true;
            var obj = JSON.parse(JSON.stringify(Intent));
            console.log(JSON.stringify(Intent));
            /*
             * {  
  "action":"android.intent.action.SEND",
  "clipItems":[  
    {  
      "text":"https://youtu.be/eLXHLRa37_g"
    }
  ],
  "flags":524289,
  "type":"text/plain",
  "component":"ComponentInfo{io.eschooltogo.telanganatenthstudymaterial/io.eschooltogo.telanganatenthstudymaterial.MainActivity}",
  "extras":{  
    "android.intent.extra.TEXT":"https://youtu.be/eLXHLRa37_g",
    "android.intent.extra.SUBJECT":"Watch \"WHAT CAN YOU CUT WITH PAPER?\" on YouTube"
  }
}
             */
            /*
             * {  
  "action":"android.intent.action.SEND",
  "clipItems":[  
    {  
      "uri":"content://com.android.chrome.FileProvider/BlockedFile_30494047768331"
    }
  ],
  "flags":50855937,
  "type":"text/plain",
  "component":"ComponentInfo{io.eschooltogo.telanganatenthstudymaterial/io.eschooltogo.telanganatenthstudymaterial.MainActivity}",
  "extras":{  
    "android.intent.extra.TEXT":"https://www.smorgasburg.com/",
    "android.intent.extra.SUBJECT":"Smorgasburg",
    "org.chromium.chrome.extra.TASK_ID":37,
    "share_screenshot_as_stream":"content://com.android.chrome.FileProvider/BlockedFile_30494047768331"
  }
}
             */
            var sharedText = obj.extras['android.intent.extra.TEXT'];
            // TODO: correct type
            var sharedSubject = obj.extras['android.intent.extra.SUBJECT'];

            
            //alert(subject);
            //alert(window.location.href.includes("settings.html"));
            
            if(window.location.href.includes("settings.html")){
              //window.location="index.html?sharedText='"+sharedText+"'";
              $.mobile.changePage('index.html');
              app.initCategoryExplorer("",CATEGORY_URL,sharedText, sharedSubject);
            }
            else{
              app.initCategoryExplorer("",CATEGORY_URL,sharedText, sharedSubject);
            }
             
            //sessionStorage.sharedText=sharedText;
            
            
        }
        else{
          alert("No connection.Try again later.");
        }
      }else{
        alert("Login to create content. You can login from settings page.");
      }
        
    }
  },

   openForm : function(path,link, subject) {

  /* ------------------------- */
  /* Contact Form Interactions */
  /* ------------------------- */

    $('.contact').addClass('is-visible');
    
    if ($('#name').val().length == 0) {
      $('#name').val(subject);
    }

    if ($('#name').val().length != 0) {
      $('.name').addClass('typing');
    }
  $("#email").val("/"+path);
  if ($('#email').val().length != 0) {
      $('.email').addClass('typing');
    }
  //close popup when clicking x or off popup
  $('.cd-popup').on('click', function(event) {
    if ($(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup')) {
      event.preventDefault();
      $(this).removeClass('is-visible');
    }
  });

  //close popup when clicking the esc keyboard button
  $(document).keyup(function(event) {
    if (event.which == '27') {
      $('.cd-popup').removeClass('is-visible');
    }
  });

  /* ------------------- */
  /* Contact Form Labels */
  /* ------------------- */
  $('#name').keyup(function() {
    $('.name').addClass('typing');
    if ($(this).val().length == 0) {
      $('.name').removeClass('typing');
    }
  });

  /* ----------------- */
  /* Handle submission */
  /* ----------------- */
  $('#contactform').submit(function(e) {
     e.preventDefault();
    var name = $('#name').val();

        if (name) {

  /*
              var googleFormsURL = "https://docs.google.com/forms/d/1dHaFG67d7wwatDtiVNOL98R-FwW1rwdDwdFqqKJggBM3nFB4/formResponse";
              // replace these example entry numbers
              var spreadsheetFields = {
                "entry.212312005": name,
                "entry.1226278897": email,
                "entry.1835345325": message
              }
              $.ajax({
                url: googleFormsURL,
                data: spreadsheetFields,
                type: "POST",
                dataType: "xml",
                statusCode: {
                  0: function() {

                  },
                  200: function() {

                  }
                }
              });
              
  */    
            app.closeForm();
            apiServicesApp.createArticle(link, name, path);
            alert("The text Shared:"+link+"Filename:"+name+" at path:"+path+" at subject:"+subject);
            return true;
            
              
            

          } 
        else {
          //$('#notification-text').html('<strong>Please provide a name.</strong>');
          //$('.notification').addClass('is-visible');
          alert("Name cannot be blank");
          
        }
        return false;
  });
},

closeForm : function () {
  document.contactform.name.value = '';
  document.contactform.email.value = '';
  $('.email').removeClass('typing');
  $('.name').removeClass('typing');

  $('.cd-popup').removeClass('is-visible');
}

};

app.initialize();











