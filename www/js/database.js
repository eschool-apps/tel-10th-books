var databaseApp = {};
databaseApp.db = null;
// databaseApp.insertVideoRecord(entries[i].name, filePathWithoutExt, fileExt);
databaseApp.insertVideoRecord = function(name, path, extension) {
  databaseApp.db.transaction(function(tx) {
    tx.executeSql(
        "INSERT INTO cache_video_files(name, path, extension) VALUES (?,?,?)",
        [ name, path, extension ], databaseApp.onSuccess, databaseApp.onError);
  });
}
databaseApp.insertImageRecord = function(name, path, extension) {
  databaseApp.db.transaction(function(tx) {
    tx.executeSql(
        "INSERT INTO cache_image_files(name, path, extension) VALUES (?,?,?)",
        [ name, path, extension ], databaseApp.onSuccess, databaseApp.onError);
  });
}

/*
 * id, uuid, path, doc.type, download_url, doc.offline_file, thumbnail_url, offline_thumbnail, doc.title, description, downloaded(0/1), download_attempts(0/1/2/3)
 * id INTEGER primary key, uuid text, path text, type text, extension text, offline_file text, display_name text, description text, download_url text, thumbnail_url text, offline_thumbnail text, downloaded INTEGER DEFAULT 0, download_attempts INTEGER DEFAULT 0
 */
databaseApp.insertYAMLrecord = function(uuid, path, extension, type, offline_file, display_name, description, download_url, thumbnail_url, offline_thumbnail, downloaded, download_attempts) {
  console.log("in insertYAMLrecord "+uuid+" download_url %%"+download_url+"%%"+" thumbnail_url %%"+thumbnail_url+"%%");
  databaseApp.db
      .transaction(function(tx) {
        tx
            .executeSql(
                "INSERT INTO cache_yaml_files(uuid, path, extension, type, offline_file, display_name, description, download_url, thumbnail_url, offline_thumbnail, downloaded, download_attempts) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",
                [ uuid, path, databaseApp.filterData(extension), type, offline_file, display_name, description, databaseApp.filterData(download_url), thumbnail_url, offline_thumbnail, downloaded, download_attempts ], databaseApp.onSuccess, databaseApp.onError);
      });
}

databaseApp.filterData = function(str) {
  return str.replace(/^\s+|\s+$/g, '');
}

databaseApp.updateYAMLdownloadedRecord = function(uuid, offline_file) {
  console.log("in updateYAMLdownloadedRecord "+uuid+" offline_file %%"+offline_file+"%%");
  if(offline_file){
    console.log("updating offline_file");
    databaseApp.db
        .transaction(function(tx) {
          tx
              .executeSql(
                  "UPDATE cache_yaml_files SET downloaded = 1, offline_file = '"+offline_file+"' WHERE uuid = '"+uuid+"'", [],
                  databaseApp.onSuccess, databaseApp.onError);
        });
  }
}

databaseApp.updateYAMLofflineThumbnailRecord = function(uuid, offline_thumbnail) {
  console.log("in updateYAMLofflineThumbnailRecord "+uuid+" offline_thumbnail %%"+offline_thumbnail+"%%");
  if(offline_thumbnail){
    console.log("updating offline_thumbnail");
    databaseApp.db
        .transaction(function(tx) {
          tx
              .executeSql(
                  "UPDATE cache_yaml_files SET offline_thumbnail = '"+offline_thumbnail+"' WHERE uuid = '"+uuid+"'", [],
                  databaseApp.onSuccess, databaseApp.onError);
        });
  }
}

databaseApp.updateYAMLdownloadAttemptsRecord = function(uuid, download_attempts) {
  console.log("in updateYAMLdownloadAttemptsRecord "+uuid+" download_attempts "+ download_attempts);
  download_attempts++;
  databaseApp.db
      .transaction(function(tx) {
        tx
            .executeSql(
                "UPDATE cache_yaml_files SET download_attempts = "+download_attempts+" WHERE uuid = '"+uuid+"'", [],
                databaseApp.onSuccess, databaseApp.onError);
      });
}

databaseApp.updateYAMLresetDownloadAttemptsRecord = function(uuid) {
  console.log("in updateYAMLresetDownloadAttemptsRecord "+uuid);
  databaseApp.db
      .transaction(function(tx) {
        tx
            .executeSql(
                "UPDATE cache_yaml_files SET download_attempts = 0 WHERE uuid = '"+uuid+"'", [],
                databaseApp.onSuccess, databaseApp.onError);
      });
}

databaseApp.initDatabase = function() {
  if(databaseApp.db === null){
    databaseApp.db = window.sqlitePlugin.openDatabase({
      name : DATABASE_NAME,
      location : 'default'
    });
  }
}

databaseApp.createDatabaseDownloadsTable = function() {
  /*
   * cache_yaml_files
   * id, uuid, path, doc.type, download_url, doc.offline_file, thumbnail_url, offline_thumbnail, doc.title, description, downloaded(0/1), download_attempts(0/1/2/3)
   */
  databaseApp.db
  .transaction(function(tx) {
    tx
        .executeSql(
            'CREATE TABLE IF NOT EXISTS cache_yaml_files (id INTEGER primary key, uuid text, path text, type text, extension text, offline_file text, display_name text, description text, download_url text, thumbnail_url text, offline_thumbnail text, downloaded INTEGER DEFAULT 0, download_attempts INTEGER DEFAULT 0)',
            [], databaseApp.onSuccess, databaseApp.onError);
  });
}

databaseApp.initDatabaseTables = function() {
  databaseApp.createDatabaseDownloadsTable();
}

databaseApp.dropDatabaseDownloadsTable = function() {
  databaseApp.db.transaction(function(tx) {
    tx.executeSql("DROP TABLE IF EXISTS cache_yaml_files", [], databaseApp.onSuccess,
        databaseApp.onError);
  });
}

databaseApp.prepareCacheTables = function() {
  // Drop table
  databaseApp.db.transaction(function(tx) {
    tx.executeSql("DROP TABLE IF EXISTS cache_video_files", databaseApp.onSuccess,
        databaseApp.onError);
  });
  // Create table
  databaseApp.db
      .transaction(function(tx) {
        tx
            .executeSql(
                'CREATE TABLE IF NOT EXISTS cache_video_files (id INTEGER primary key, name text, path text, extension text)',
                databaseApp.onSuccess, databaseApp.onError);
      });

  // Drop table
  databaseApp.db.transaction(function(tx) {
    tx.executeSql("DROP TABLE IF EXISTS cache_image_files", databaseApp.onSuccess,
        databaseApp.onError);
  });
  // Create table
  databaseApp.db
      .transaction(function(tx) {
        tx
            .executeSql(
                'CREATE TABLE IF NOT EXISTS cache_image_files (id INTEGER primary key, name text, path text, extension text)',
                databaseApp.onSuccess, databaseApp.onError);
      });

  // Drop table
  databaseApp.db.transaction(function(tx) {
    tx.executeSql("DROP TABLE IF EXISTS cache_yaml_files", databaseApp.onSuccess,
        databaseApp.onError);
  });
  // Create table
  /*
   * type: video/article
   */
  databaseApp.db
      .transaction(function(tx) {
        tx
            .executeSql(
                'CREATE TABLE IF NOT EXISTS cache_yaml_files (id INTEGER primary key, name text, path text, unified_path text, extension text, type text, offline_file text, display_name text, description text)',
                databaseApp.onSuccess, databaseApp.onError);
      });
}

databaseApp.onSuccess = function(tx, r) {
  // console.log("Your SQLite query was successful!");
}

databaseApp.onError = function(tx, e) {
  console.log("SQLite Error: " + JSON.stringify(e));
  // SQLite Error: {"rows":{"length":0},"rowsAffected":0}
  if (e.message) {
    navigator.notification.alert("SQLite Error: " + e.message);
  }
}
